﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TLDR.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SourceController : ControllerBase
    {
        private string baseUrl = "http://tldr-es.jcloud-ver-jpe.ik-server.com/";

        // GET api/<SourceController>/5
        [HttpGet("{url}")]
        public string Get(string url)
        {
            return "value";
        }

        private async Task<string> GetTLDRBySource(string value)
        {
            string apiResponse = string.Empty;
            var httpContent = new StringContent(value, Encoding.UTF8, "application/json");
            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.PutAsync(baseUrl + "source/", httpContent))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                }
            }
            return apiResponse = "test3";
        }
    }
}
