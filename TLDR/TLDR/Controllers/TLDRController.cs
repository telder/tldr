﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TLDR.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TLDRController : ControllerBase
    {
        private string baseUrl = "http://tldr-es.jcloud-ver-jpe.ik-server.com/";

        // GET: <TLDRController>
        [HttpGet]
        public async Task<string> Get()
        {
            return await this.GetAllTLDR();
        }

        // GET <TLDRController>/5
        [HttpGet("{id}")]
        public async Task<string> GetAsync(string id)
        {
            return await this.GetTLDRByIdAsync(id);
        }

        // POST <TLDRController>
        [Authorize]
        [HttpPost]
        public async Task<string> Post([FromBody] TLDR value)
        {
            return await this.CreateTLDR(value);
        }

        // PUT <TLDRController>/5
        [Authorize]
        [HttpPatch("{id}")]
        public async void Patch(string id, [FromBody] TLDR value)
        {
            TLDR tldr = new TLDR
            {
                tldr_id = id,
                title = value.title,
                author = value.author,
                summary = value.summary,
                categories = value.categories,
                source_url = value.source_url,
                created_date = value.created_date,
                updated_by = value.updated_by,
            };
            this.UpdateTLDR(tldr);
        }

        // DELETE <TLDRController>/5
        [Authorize]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        //Elastic Search call API
        private async Task<string> GetAllTLDR()
        {
            string apiResponse = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.GetAsync(baseUrl + "tldr_list/"))
                {
                    apiResponse = await response.Content.ReadAsStringAsync();
                }
            }
            return apiResponse = "test";
        }

        private async Task<string> GetTLDRByIdAsync(string id)
        {
            string responseBody;
            string jsonContent = "{\"tldr_id\":\"" + id + "\"}";

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(baseUrl + "tldr"),
                Content = new StringContent(jsonContent, Encoding.UTF8, "application/json"),
            };

            var response = await client.SendAsync(request).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();

            responseBody = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            responseBody.Replace("id", "tldr_id");

            return responseBody;
        }

        private async Task<string> CreateTLDR(TLDR value)
        {
            string responseBody;
            string jsonContent = "{\"title\":\"" + value.title + "\"," +
                "\"summary\":\"" + value.summary + "\"," +
                "\"author\":\"" + value.author + "\"," +
                "\"categories\":" + Newtonsoft.Json.JsonConvert.SerializeObject(value.categories) + "," +
                "\"source_url\":\"" + value.source_url + "\"}";

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(baseUrl + "tldr"),
                Content = new StringContent(jsonContent, Encoding.UTF8, "application/json"),
            };

            var response = await client.SendAsync(request).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();

            responseBody = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return responseBody;
        }

        private async void UpdateTLDR(TLDR value)
        {

            string responseBody;
            string jsonContent = "{\"tldr_id\":\"" + value.tldr_id + "\"," +
                "\"title\":\"" + value.title + "\"," +
                "\"summary\":\"" + value.summary + "\"," +
                "\"categories\":" + Newtonsoft.Json.JsonConvert.SerializeObject(value.categories) + "," +
                "\"source_url\":\"" + value.source_url + "\"," +
                "\"updated_by\":\"" + value.updated_by + "\"}";

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Patch,
                RequestUri = new Uri(baseUrl + "tldr"),
                Content = new StringContent(jsonContent, Encoding.UTF8, "application/json"),
            };

            var response = await client.SendAsync(request).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
        }
    }
}
