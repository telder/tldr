﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TLDR
{
    public class TLDR
    {
        public string tldr_id { get; set; }

        public string title { get; set; }

        public string summary { get; set; }

        public string author { get; set; }

        public string source_url { get; set; }

        public List<string> likes { get; set; }

        public List<string> categories { get; set; }

        public DateTime created_date { get; set; }

        public DateTime updated_date { get; set; }

        public string updated_by { get; set; }
    }
}
